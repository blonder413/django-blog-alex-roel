# Instalar django
```
pip install django
```
# Crear proyecto django
```
django-admin startproject blog-personal
```

# Crear aplicaciones
```
python3 manage.py startapp core
python3 manage.py startapp blog
python3 manage.py startapp perfil
```

# Inicial el servidor
```
python3 manage.py runserver
```

# Correr migraciones
Podemos crear las migraciones de cada uno de nuestros modelos
```
python3 manage.py makemigrations blog
```
También podemos crear las migraciones de todos los modelos de una vez
```
python3 manage.py migrate
```
Luego de crearlas debemos ejecutarlas
```
python3 manage.py migrate
```
Si deseamos ver el sql de cada migración podemos revisar dentro del directorio ```m̀igrations```
de cada app y ejecutar el siguiente comando según el número correspondiente
```
python3 manage.py sqlmigrate blog 0001
```

## Instalar una app
Agregamos la configuración dentro del archivo ```settings.py``` en el bloque ```INSTALLED_APPS```.
Esta configuración está en el archivo ```apps.py``` de cada app
```
'core.apps.CoreConfig',
'blog.apps.BlogConfig',
'perfil.apps.PerfilConfig',
```

## Crear superusuario
```
python3 manage.py createsuperuser
```


## CKEditor
[ckeditor](https://django-ckeditor.readthedocs.io/en/latest/)
```
pip install django-ckeditor
```