from django.shortcuts import render
from .models import Post


# Create your views here.
def index(request):
    """
    Listar todos los posts
    :param request:
    :return: HttpResponse
    """
    # posts = Post.objects.order_by('-created').all()
    posts = Post.objects.all()
    recientes = Post.objects.all()[:3]
    return render(request, 'index.html', {'posts': posts, 'recientes': recientes})


def view(request, id):
    """
    Listar un post
    :return:
    """
    post = Post.objects.get(id=id)
    recientes = Post.objects.order_by('-created').all()[:3]
    return render(
        request,
        'view.html',
        {'post': post, 'recientes': recientes})
