from django.db import models
from ckeditor.fields import RichTextField


# Create your models here.
class Post(models.Model):
    image = models.ImageField(upload_to='blog/', verbose_name='Imagen')
    title = models.CharField(max_length=200, verbose_name='Título')
    desc = models.TextField(verbose_name='Descripción')
    content = RichTextField(verbose_name='Contenido')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Creado')
    updated = models.DateTimeField(auto_now=True, verbose_name='Actualizado')

    class Meta:
        verbose_name = 'Artículo'
        verbose_name_plural = 'Artículos'
        ordering = ['-created']
