from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='blog_index'),
    path('<int:id>/', views.view, name='blog_view'),
]